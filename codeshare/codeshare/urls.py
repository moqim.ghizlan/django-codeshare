from django.contrib import admin
from django.urls import path, include
import core.views as views
urlpatterns = [
    path('admin/', admin.site.urls),

    path('', include('core.urls')),
    # path('', views.index, name="index"),
    # path('<slug>', views.code_with_slug, name="code_with_slug"),
    # path('code/create', views.new_code, name="new_code"),
    path('api/', include('api.urls'))
]
