from django.db import models

import random
import string

def auto_gen_slug():
    allowed_chars = string.ascii_uppercase + string.digits
    return  ''.join(random.choices(allowed_chars, k=8))

class Code(models.Model):
    slug = models.SlugField( unique=True, help_text='A short label containing only letters, numbers, underscores or hyphens.', max_length=255, verbose_name='Slug', default=auto_gen_slug, editable=False)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True, verbose_name='Created')
    updated = models.DateTimeField( auto_now=True, verbose_name='Updated')

    class Meta:
        verbose_name = 'Code'
        verbose_name_plural = 'Codes'
        ordering = ('-created',)
    def __str__(self):
        return self.slug





