from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import render, redirect
from .models import Code

def index(request):
    if request.method == 'POST':
        slug = request.POST.get("room-slug")
        return redirect('code_with_slug', slug = slug)
    return render(request, 'lobby.html')

import json
def code_with_slug(request, slug):
    if request.method == 'POST':
        data = json.loads(request.body)
    code, created = Code.objects.get_or_create(slug = slug)
    return render(request, 'show.html', {'code': code})

def new_code(request):
    code = Code.objects.create()
    return redirect(
        'code_with_slug',
        slug = code.slug
    )


def api(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        slug = data['slug']
        content = data['content']
        return JsonResponse({'success': True})
    else:
        return HttpResponseBadRequest('Invalid request method')