
from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name="index"),
    path('<slug>', views.code_with_slug, name="code_with_slug"),
    path('code/create', views.new_code, name="new_code"),
    # path('api', views.api, name="api")
]
