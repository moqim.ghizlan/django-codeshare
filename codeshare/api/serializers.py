from rest_framework import serializers
from core.models import Code


class CodeSerializers(serializers.ModelSerializer):
    class Meta(object):
        model = Code
        fields = '__all__'
