from rest_framework.response import Response
from rest_framework.decorators import api_view
from core.models import Code
from .serializers import CodeSerializers
#import django http Response
from django.http import HttpResponse, JsonResponse
@api_view(['POST', "GET"])
def getDate(request):
    code = Code.objects.all()
    serializer = CodeSerializers(code, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def edit_code(request, code_slug):
    data = request.data
    code = Code.objects.filter(slug = code_slug).first()
    if not code:

        return JsonResponse(
        {
            'code' :  0,
            "message" : 'Code dos not exists'
         }, safe=False)
    code.content = data['content']
    code.save()
    serializer = CodeSerializers(code, many=False)
    return JsonResponse(
        {
            'code' :  1,
            "message" : 'ok',
            "object" : serializer.data
         }, safe=False)

