from django.urls import path
from . import views
urlpatterns = [
    path('', views.getDate),
    path('code/slug=<slug:code_slug>/edit', views.edit_code),
    # path('code/<slug:code_slug>/edit', views.edit_code)
]